$(document).foundation();

/*********************
NAVEGACION
*********************/
var screenHeight = $(window).height(),
	screenWidth = $(window).width();

	$(document).ready(function(){

	    $(window).resize(function(){
	        screenHeight = $(window).height();
	        screenWidth = $(window).width();
	    });
	    $(window).on('scroll',function() {
	      var scrolltop = $(this).scrollTop();
	   
	      if(scrolltop >= screenHeight) {
	        $('#header__navegacion-fijo, #header__menu-mobile').fadeIn(250);
	      }
	      
	      else if(scrolltop <= screenHeight) {
	        $('#header__navegacion-fijo, #header__menu-mobile').fadeOut(250);
	      }
	    });

	    $("#header__main-menu a[href^='#'], "+
	      "#header__main-menu img[href^='#'], "+
		  "#header__navegacion-fijo a[href^='#'], "+
	      "#header__navegacion-fijo img[href^='#'], "+
	      "#header__menu-mobile a[href^='#'], "+
	      "#header__menu-mobile img[href^='#'], "+
	      "#header__arrow-menu a[href^='#'] ").on('click', function(e) {
	          // prevent default anchor click behavior
	          e.preventDefault();
	          // store hash
	          var hash = this.hash;
	           // animate
	           var navigationHeight = $("#header__navegacion-fijo").outerHeight();
	           var offset=  $(hash).offset().top;
	           if($(this).attr('href')=="#home" || $(this).attr('href')=="#nosotros"){
	              offset = offset + navigationHeight;
	           }
	           //*
	           $('html,body').animate({
	             "scrollTop": offset - navigationHeight
	           }, 750, function(){
	             // when done, add hash to url
	             // (default click behaviour)
	             window.location.hash = hash;
	           });
	      });

        /*********************
        SHOW/HIDE 
        *********************/

        var contacto_content = {

          eeuu:'<div class="" id="eeuu" data-toggler="hide"><h4>EEUU - Miami</h4><ul class="no-bullet"><li><h6>Dirección:</h6>8425 NW, 68th Street, Miami, FL 33166, U.S.A.</li><li><h6>Teléfonos:</h6>(305) 477.30.11 / 477.30.77</li><li><h6>Fax:</h6>(305) 675.23.73</li><li><h6>Email:</h6> tracking@rapicar.com</li></ul></div>',
          china:'<div class="" id="china" data-toggler="hide"><h4>China - Guangzhou</h4><ul class="no-bullet"><li><h6>Dirección:</h6>Room 15-I, Wing Kin Square, Nro. 29,Jianshe Liu Ma Lu, Yuexiu District,Guangzhou, PR-CHINA,Postal Code 510060</li><li><h6>Teléfonos:</h6>(+86) 20 83324827</li><li><h6>Fax:</h6>(+86) 20 83314942</li><li><h6>Email:</h6> trading@rapicar.com</li></ul></div>',
          panama:'<div class="" id="panama" data-toggler="hide"><h4>PANAMA</h4><ul class="no-bullet"><li><h6>Dirección:</h6>Room 15-I, Wing Kin Square, Nro. 29,Jianshe Liu Ma Lu, Yuexiu District,Guangzhou, PR-CHINA,Postal Code 510060</li><li><h6>Teléfonos:</h6>(+86) 20 83324827</li><li><h6>Fax:</h6>(+86) 20 83314942</li><li><h6>Email:</h6> trading@rapicar.com</li></ul>   </div>',
          guatemala:'<div class="" id="guatemala" data-toggler="hide"><h4>GUATEMALA</h4><ul class="no-bullet"><li><h6>Dirección:</h6>Room 15-I, Wing Kin Square, Nro. 29,Jianshe Liu Ma Lu, Yuexiu District,Guangzhou, PR-CHINA,Postal Code 510060</li><li><h6>Teléfonos:</h6>(+86) 20 83324827</li><li><h6>Fax:</h6>(+86) 20 83314942</li><li><h6>Email:</h6> trading@rapicar.com</li></ul>   </div>',
          eeuu_map: '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3591.038667777941!2d-80.3368520855688!3d25.8352711118581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9bbc7bd3be45d%3A0xb4bc917754dff577!2s8425+NW+68th+St%2C+Miami%2C+FL+33166%2C+EE.+UU.!5e0!3m2!1ses!2sve!4v1460339039447" width="100%" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>',
          china_map: '<iframe data-toggler="hide" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7337.975281993293!2d113.28121887261712!3d23.13412700571925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3402ff2c9457bb3b%3A0xc6dff6625434b12!2sYuexiu%2C+Cant%C3%B3n%2C+Provincia+de+Cant%C3%B3n%2C+China%2C+510060!5e0!3m2!1ses!2sve!4v1460339321923" width="100%" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>'

        };

        var inject_content = function(id){
          $('#contacto__direccion').html(contacto_content[id]);
          $('#contacto__map').html(contacto_content[id+'_map']);
          // $('#contacto__map').html(contacto_content['eeuu_map']);
        };


        $('#pin-eeuu').click(function (){
          inject_content('eeuu');
        });

        $('#pin-china').click(function (){
          inject_content('china');
        });

        $('#pin-panama').click(function (){
          inject_content('panama');
        });

        $('#pin-guatemala').click(function (){
          inject_content('guatemala');
        });

        $('#flag-eeuu').click(function (){
          inject_content('eeuu');
        });

        $('#flag-china').click(function (){
          inject_content('china');
        });

        $('#flag-panama').click(function (){
          inject_content('panama');
        });

        $('#flag-guatemala').click(function (){
          inject_content('guatemala');
        });


	});


/*********************
API SOCIAL MEDIA
*********************/
  // Orbit
  $(document).ajaxComplete(function (event, xhr, settings){
    var $elem = new Foundation.Orbit($("#evento__slider"), {
    });
  });
  // social feed
  $.ajax({
      url: "http://www.inworknet.com:3080/api/post",
      headers: {"Token": "123456"},
      success: function(ejemplo) { 
        //setTimeout(function(){
        //  $("#evento__loader").remove();
        //},5000);
           // console.log(ejemplo); 
          // var ejemplo=[
          //      {
          //          "message": "este es facebook",
          //          "type": "facebook",
          //          "date": 14505065060560
          //      },
          //      {
          //          "message": "este es instagram",
          //          "type": "instagram",
          //          "image": "http://www.tresensocial.com/3es/wp-content/uploads/2015/09/Instagram.png",
          //          "date": 14505099060560    
          //      },
          //      {
          //          "message": "este es twitter",
          //          "type": "twitter",
          //          "date": 14505076060560
          //      }
          //  ];

          console.log(ejemplo);
         var evento__li="";
         // ciclo
         for(var i=0; i<ejemplo.length;i++){

          var k = i + 1;

          var StringToAdd = '<span class="fa-stack fa-lg"> ';
          StringToAdd += '<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>';
          StringToAdd += '<i class="fa fa-'+ejemplo[i].type+' fa-stack-1x fa-inverse" aria-hidden="true"></i>';
          StringToAdd += '</span>'; 

          if(ejemplo[i].image) StringToAdd = '<img src="'+ejemplo[i].image+'" /> ';

          StringToAdd += '<p>'+ejemplo[i].message+'</p>';

          $("#evento__"+k).html(StringToAdd);

         } // /. ciclo
         //console.log(evento__li);
         // $('#evento__arreglo').append(evento__li);
         //$(evento__li).appendTo('#evento__arreglo');

      }    
    }); // /. social feed

/*********************
SENDGRID
*********************/

$('input').on('input',function(){
     $(this).css('color','rgb(82, 82, 82)');
   });

   if (!String.prototype.splice) {
       /**
        * {JSDoc}
        *
        * The splice() method changes the content of a string by removing a range of
        * characters and/or adding new characters.
        *
        * @this {String}
        * @param {number} start Index at which to start changing the string.
        * @param {number} delCount An integer indicating the number of old chars to remove.
        * @param {string} newSubStr The String that is spliced in.
        * @return {string} A new string with the spliced substring.
        */
       String.prototype.splice = function(start, delCount, newSubStr) {
           return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
       };
   }

    $("#form").submit(function (e) {
        e.preventDefault();
        var name = $("#nombre").val();
        var email = $("#email").val();
        var paisDestino = $("#pais-destino").val();
        var paisOrigen = $("#pais-origen").val();
        var subject = $("#subject").val();
        var text = $("#text").val();
        var subscriberData = {
          email: email,
          name: name
        };
        var dataString ='api_user=acavadia'+'&api_key=InWork123'+ '&to=jesuspoleo18@gmail.com' + '&toname=rapicar' + '&subject=' + subject + '&from='+email + '&text=' + text ;

        var dataString2 ='api_user=acavadia'+'&api_key=InWork123'+'&list=rapicar' + '&data=' + JSON.stringify(subscriberData);

        function isValidEmail(emailAddress) {
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            return pattern.test(emailAddress);
        };

        if (isValidEmail(email) && (name.length > 1)) {
          $.ajax({
            type: "POST",
            url: "https://api.sendgrid.com/api/newsletter/lists/email/add.json",
            data: dataString2,
            beforeSend: function () {
              $("#form-fijo")
                .LoadingOverlay("show", {
                  image: '/img/loader.gif'
                });
            },
            complete: function () {
              var notymsg;
              $.ajax({
                type: "POST",
                url: "https://api.sendgrid.com/api/mail.send.json",
                data: dataString,
                complete: function () {
                  $("#form-fijo")
                    .LoadingOverlay("hide");
                  $("#nombre")
                    .val('');
                  $("#email")
                    .val('');
                  $("#pais-destino")
                    .val('');
                  $("#pais-origen")
                  .val('');
                  $("#subject")
                    .val('');
                  $("#text")
                  .val('');
                  notymsg="Your message has been sent to Erasmo's email";

                  var n = noty({
                    text: notymsg,
                    type: 'success',
                    animation: {
                      open: {height: 'toggle'}, // Animate.css class names
                      close: {height: 'toggle'}, // Animate.css class names
                      easing: 'swing', // unavailable - no need
                      speed: 500 // unavailable - no need
                    }
                  });
                }
              });
            }
          });
        } else {
          notymsg="Excuse us, try again with a different email, or do you forgot to write your Name?";
          var n = noty({
            text: notymsg,
            type: 'error',
            animation: {
              open: {height: 'toggle'}, // Animate.css class names
              close: {height: 'toggle'}, // Animate.css class names
              easing: 'swing', // unavailable - no need
              speed: 500 // unavailable - no need
            }
          });
        }

        return false;
    });